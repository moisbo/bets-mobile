var gulp = require('gulp'),
    gulp_concat = require('gulp-concat'),
    gulp_rename = require('gulp-rename');

gulp.task('build', function() {
    var bads = 'bads/www/';
    gulp.src(['vendor/jquery/dist/jquery.min.js',
        'vendor/angular/angular.min.js',
        'vendor/angular-route/angular-route.min.js',
        'vendor/mobile-angular-ui/dist/js/mobile-angular-ui.min.js',
        'vendor/mobile-angular-ui/dist/js/mobile-angular-ui.gestures.min.js'])
        .pipe(gulp.dest(bads+'js/vendor'));
    gulp.src(['vendor/mobile-angular-ui/dist/css/mobile-angular-ui-hover.min.css',
        'vendor/mobile-angular-ui/dist/css/mobile-angular-ui-base.min.css',
        'vendor/mobile-angular-ui/dist/css/mobile-angular-ui-desktop.min.css'])
        .pipe(gulp.dest(bads+'css/vendor'));
    gulp.src(['vendor/mobile-angular-ui/dist/fonts/fontawesome-webfont.woff2',
        'vendor/mobile-angular-ui/dist/fonts/fontawesome-webfont.woff',
        'vendor/mobile-angular-ui/dist/fonts/fontawesome-webfont.ttf'])
        .pipe(gulp.dest(bads+'css/fonts'));
    gulp.src(['js/**'])
        .pipe(gulp_concat('app.js'))
        .pipe(gulp.dest(bads+'js'))
});

gulp.task('watch', function () {
    gulp.watch(['js/**/*.js'], ['build']);
});

gulp.task('default', ['build', 'watch']);