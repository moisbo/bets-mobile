app.controller('DepositCtrl', function ($rootScope, $scope, $http, $location, pageInfo) {
    $('.navbar-brand').html('News');
    $http
        .get(settings.url + settings.urls.deposit)
        .success(function(res){
            $scope.data = res;
            console.log(res);

        });
    $scope.clickInfo = function(d){
        pageInfo.set(d);
        $location.path('pageInfo');
    };
});