app.controller('RacingCtrl', function ($rootScope, $scope, $http) {
    $('.navbar-brand').html('Racing');
    $http
        .get(settings.url + settings.urls.racing)
        .success(function(res){
            $scope.data = res;
        });
});