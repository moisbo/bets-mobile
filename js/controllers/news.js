app.controller('NewsCtrl', function ($rootScope, $scope, $http) {
    $('.navbar-brand').html('News');
    $http
        .get(settings.url + settings.urls.news)
        .success(function(res){
            $scope.data = res;
        });
});