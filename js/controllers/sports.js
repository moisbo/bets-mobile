app.controller('SportsCtrl', function ($rootScope, $scope, $http) {
    $('.navbar-brand').html('Sports');
    $http
        .get(settings.url + settings.urls.sports)
        .success(function(res){
            $scope.data = res;
        });
});