'use strict';

// Here is how to define your module
// has dependent on mobile-angular-ui
//
var app = angular.module('App', [
    'ngRoute',
    'mobile-angular-ui',

    // touch/drag feature: this is from 'mobile-angular-ui.gestures.js'
    // it is at a very beginning stage, so please be careful if you like to use
    // in production. This is intended to provide a flexible, integrated and and
    // easy to use alternative to other 3rd party libs like hammer.js, with the
    // final pourpose to integrate gestures into default ui interactions like
    // opening sidebars, turning switches on/off ..
    'mobile-angular-ui.gestures'
]);

app.run(function($transform) {
    window.$transform = $transform;
});

//
// You can configure ngRoute as always, but to take advantage of SharedState location
// feature (i.e. close sidebar on backbutton) you should setup 'reloadOnSearch: false'
// in order to avoid unwanted routing.
//
app.config(function($routeProvider) {
    $routeProvider.when('/', {templateUrl: 'news.html', reloadOnSearch: false});
    $routeProvider.when('/deposit', {templateUrl: 'deposit.html', reloadOnSearch: false});
    $routeProvider.when('/racing', {templateUrl: 'racing.html', reloadOnSearch: false});
    $routeProvider.when('/sports', {templateUrl: 'sports.html', reloadOnSearch: false});
    $routeProvider.when('/pageInfo', {templateUrl: 'pageInfo.html', reloadOnSearch: false});
});