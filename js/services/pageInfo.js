app.factory('pageInfo', function() {
    var data = {};
    return {
        set: function (d) {
            data = d;
        },
        get: function () {
            return data;
        }
    }
});